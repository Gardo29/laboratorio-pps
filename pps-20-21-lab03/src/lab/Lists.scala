package lab
import u02.Modules.Person
import u02.Modules.Person.Teacher
import u03.Lists.List._
import u03.Lists._
import u02.Optionals.Option._
import u02.Optionals.Option

import scala.annotation.tailrec

object Lists {

  @tailrec
  def drop[A](l:List[A], n:Int) : List[A] = l match {
    case Cons(_,t) if n>0 => drop(t,n-1)
    case _ if n < 0 => Nil()
    case _ => l
  }

  def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
    case Cons(h,t) => append(f(h),flatMap(t)(f))
    case _ => Nil()
  }

  def filter[A](l: List[A])(predicate: A => Boolean) : List[A] = flatMap(l)({
    case h if predicate(h) => Cons(h,Nil())
    case _ => Nil()
  })

  def map[A,B](l: List[A])(mapper: A => B): List[B] = flatMap(l)(x => Cons(mapper(x),Nil()))

  def max(l: List[Int]): Option[Int] = l match {
    case Cons(h,t) => max(t) match {
      case Some(a) if a > h => Some(a)
      case _ => Some(h)
    }
    case _ => None()
  }

  def max3(l: List[Int]): Option[Int] = l match {
    case Cons(h,t) if h >= getOrElse(max(t),h) => Some(h)
    case Cons(_,t) => max(t)
    case _ => None()
  }

  @tailrec
  def max2(l: List[Int]): Option[Int] = l match {
    case Cons(h,t) => filter(t)(_>h) match {
      case Nil() => Some(h)
      case _ => max2(t)
    }
    case _ => None()
  }

  def extractCourses(l:List[Person]):List[String] = flatMap(l)({
    case Teacher(_, course) => Cons(course,Nil())
    case _ => Nil()
  })

  @tailrec
  def foldLeft[A,B](l:List[A])(startValue:B)(binaryOperator: (B,A)=>B): B = l match {
    case Cons(h,t) => foldLeft(t)(binaryOperator(startValue,h))(binaryOperator)
    case _ => startValue
  }

  def foldRight[A,B](l:List[A])(startValue:B)(binaryOperator: (A,B)=>B): B = l match {
    case Cons(h,t) => binaryOperator(h,foldRight(t)(startValue)(binaryOperator))
    case _ => startValue
  }


}
