package lab
import u03.Streams.Stream
import u03.Streams.Stream.{Cons, cons}

import scala.annotation.tailrec
object Streams {

  @tailrec
  def drop[A](s:Stream[A])(amount: Int):Stream[A] = s match {
    case Cons(_,t) if amount > 0 => drop(t())(amount-1)
    case _ => s
  }

  def constant[A](seed: A): Stream[A] = cons(seed,constant(seed))

  def infFibonacci() : Stream[Int] = {
    def _iterate(prev:Int,next:Int): Stream[Int] = cons(prev,_iterate(next,prev+next))
    _iterate(0,1)
  }
}
