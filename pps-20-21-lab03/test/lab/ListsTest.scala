package lab

import u02.Optionals.Option.{Some,None}
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u03.Lists.List.{filter,map,Cons,Nil}
import u03.Lists.List
import u02.Modules.Person
import u02.Modules.Person._
import lab.Lists._

class ListsTest {

  private val emptyList:List[Int] = Nil()
  private val lst =  Cons (10 , Cons (20 , Cons (30 , Nil ())))

  @Test
  def testDropOneElement(): Unit ={
    assertEquals(Cons (20 , Cons (30 , Nil ())),drop(lst,1))
  }

  @Test
  def testDropTwoElements(): Unit ={
    assertEquals(Cons (30 , Nil ()),drop(lst,2))
  }

  @Test
  def testDropTooManyElements(): Unit ={
    assertEquals(emptyList,drop(lst,5))
  }

  @Test
  def testDropNegativeElements(): Unit ={
    assertEquals(emptyList,drop(lst,-5))
  }

  @Test
  def testFlatMapWithValues(): Unit ={
    assertEquals(Cons (11 , Cons (21 , Cons (31 , Nil ()))),flatMap ( lst )(v => Cons ( v +1 , Nil () )) )
  }

  @Test
  def testFlatMapEmptyList(): Unit ={
    assertEquals(emptyList,flatMap ( emptyList )(v => Cons ( v + 1 , Nil () )) )
  }

  @Test
  def testMapWithValues(): Unit ={
    assertEquals(Cons("10",Cons("20",Cons("30",Nil()))),map(lst)(x => x.toString))
  }

  @Test
  def testMapWithEmptyList(): Unit ={
    assertEquals(emptyList,map(emptyList)(x => x.toString))
  }


  @Test
  def testMaxWithValue(): Unit ={
    assertEquals(Some (25),max ( Cons (10 , Cons (25 , Cons (20 , Nil () ) ) ) ))
  }

  @Test
  def testMax2WithValue(): Unit ={
    assertEquals(Some (25),max2 ( Cons (10 , Cons (25 , Cons (20 , Nil () ) ) ) ))
  }

  @Test
  def testMax3WithValue(): Unit ={
    assertEquals(Some (25),max3 ( Cons (10 , Cons (25 , Cons (20 , Nil () ) ) ) ))
  }

  @Test
  def testMaxWithNoValue(): Unit ={
    assertEquals(None(),max (emptyList))
  }

  @Test
  def testFilterWithOneValue(): Unit ={
    assertEquals(Cons(10,Nil()),filter(lst)(_<11))
  }

  @Test
  def testFilterWithMultipleValues(): Unit ={
    assertEquals(Cons(10,Cons(20,Nil())),filter(lst)(_<30))
  }

  @Test
  def testFilterWithImpossibleFilter(): Unit ={
    assertEquals(emptyList,filter(lst)(_<5))
  }

  @Test
  def testExtractCoursesHeterogeneousUniversityList(): Unit ={
    val heterogeneousUniversityList:List[Person] = Cons(Teacher("Viroli","OOP"),Cons(Student("Giammarco",1),Cons(Teacher("Ricci","IOT"),Nil())))
    assertEquals(Cons("OOP",Cons("IOT",Nil())),extractCourses(heterogeneousUniversityList))
  }

  @Test
  def testExtractCoursesHomogeneousUniversityList(): Unit ={
    val heterogeneousUniversityList:List[Person] = Cons(Teacher("Viroli","OOP"),Cons(Teacher("Maltoni","ML"),Cons(Teacher("Ricci","IOT"),Nil())))
    assertEquals(Cons("OOP",Cons("ML",Cons("IOT",Nil()))),extractCourses(heterogeneousUniversityList))
  }

  @Test
  def testExtractCoursesHomogeneousInvalidUniversityList(): Unit ={
    val heterogeneousUniversityList:List[Person] = Cons(Student("Yuqi",1),Cons(Student("Ismam",1),Cons(Student("Davide",1),Nil())))
    assertEquals(emptyList,extractCourses(heterogeneousUniversityList))
  }

  @Test
  def testFoldLeftPlus(): Unit ={
    assertEquals(60,foldLeft(lst)(0)(_+_))
  }

  @Test
  def testFoldLeftMinus(): Unit ={
    assertEquals(-60,foldLeft(lst)(0)(_-_))
  }

  @Test
  def testFoldLeftDiv(): Unit ={
    val lst =  Cons (2 , Cons (2 , Cons (2 , Cons (2 , Nil ()))))
    assertEquals(1,foldLeft(lst)(16)(_/_))
  }

  @Test
  def testFoldRightPlus(): Unit ={
    assertEquals(60,foldRight(lst)(0)(_+_))
  }

  @Test
  def testFoldRightMinus(): Unit ={
    assertEquals(20,foldRight(lst)(0)(_-_))
  }

  @Test
  def testFoldRightDiv(): Unit ={
    val lst =  Cons (16 , Cons (8 , Cons (4 ,Nil ())))
    assertEquals(8,foldRight(lst)(1)(_/_))
  }



}
