package lab
import lab.Streams.{constant, drop, infFibonacci}
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u03.Streams.Stream
import u03.Streams.Stream.{Empty, take, toList}
import u03.Lists.List.{Cons, Nil}
import u03.Lists.List
class StreamsTest{

  private val emptyStream:Stream[Int] = Empty()
  private val emptyStreamToList:List[Int] = Nil()
  private val stream = Stream.take(Stream.iterate(0)(_+1))(10)

  @Test
  def testDropOnValueStream(): Unit ={
    assertEquals(Cons (6 , Cons (7 , Cons (8 , Cons (9 , Nil ())))),toList(drop(stream)(6)))
  }

  @Test
  def testDropOnEmptyStream(): Unit ={
    assertEquals(emptyStreamToList,toList(drop(emptyStream)(6)))
  }

  @Test
  def testConstantTakeOne(): Unit ={
    assertEquals(Cons("prova",Nil()),toList(take(constant("prova"))(1)))
  }

  @Test
  def testConstantTakeOneEmptyStream(): Unit ={
    assertEquals(emptyStreamToList,toList(take(emptyStream)(1)))
  }

  @Test
  def testConstantTakeXFiveTimes(): Unit ={
    assertEquals(Cons ("x", Cons ("x", Cons ("x", Cons ("x", Cons ("x", Nil ()))))),toList(take(constant("x"))(5)))
  }
  @Test
  def testConstantTakeXFiveTimesEmptyList(): Unit ={
    assertEquals(Cons (Nil(), Cons (Nil(), Cons (Nil(), Cons (Nil(), Cons (Nil(), Nil ()))))),toList(take(constant(Nil()))(5)))
  }

  @Test
  def testInfFibonacciTakeEight(): Unit ={
    val fibonacciTen = Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Nil()))))))))
      assertEquals(fibonacciTen, toList(take(infFibonacci())(8)))
  }

}
