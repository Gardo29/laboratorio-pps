package u05lab.code

import u05lab.code.ExamsManagerTest.ExamResult.Kind.Kind

object ExamsManagerTest extends App {


  sealed trait ExamResult{

    def kind:Kind

    def evaluation:Option[Int]

    def cumLaude:Boolean

  }


  object ExamResult {

    object Kind {

      sealed trait Kind

      case object RETIRED extends Kind

      case object FAILED extends Kind

      case object SUCCEEDED extends Kind

    }

    private case class ExamResultImpl(kind: Kind, evaluation: Option[Int], cumLaude: Boolean) extends ExamResult{
      override def toString: String = this.evaluation match {
        case Some(n) if n == 30 && cumLaude => kind.toString +"(30L)"
        case Some(n) => kind.toString +"("+n+")"
        case _ => kind.toString
      }
    }

    def failed: ExamResult = ExamResultImpl(Kind.FAILED, Option.empty, cumLaude = false)

    def retired: ExamResult = ExamResultImpl(Kind.RETIRED, Option.empty, cumLaude = false)

    def succeededCumLaude: ExamResult = ExamResultImpl(Kind.SUCCEEDED, Option(30), cumLaude = true)

    def succeeded(evaluation: Int): ExamResult = evaluation match {
      case n if 18 <= n && n <= 30 => ExamResultImpl(Kind.SUCCEEDED, Option(evaluation), cumLaude = false)
      case _ => throw new IllegalArgumentException
    }
  }

  sealed trait ExamsManager {
    def createNewCall(call: String): Unit

    def addStudentResult(call: String, student: String, result: ExamsManagerTest.ExamResult): Unit

    def getAllStudentsFromCall(call: String): Set[String]

    def getEvaluationsMapFromCall(call: String): Map[String,Int]

    def getResultsMapFromStudent(student: String): Map[String,String]

    def getBestResultFromStudent(student: String): Option[Int]
  }

  object ExamsManager{

    def apply(): ExamsManager = ExamManagerImp()

    case class ExamManagerImp() extends ExamsManager{
      private var calls:Map[String,Map[String,ExamResult]] = Map()

      override def createNewCall(call: String): Unit = call match {
        case c if this.calls.contains(c) => throw new IllegalArgumentException
        case _ => this.calls += call -> Map()
      }

      override def addStudentResult(call: String, student: String, result: ExamResult): Unit = (call,student) match {
        case (c,s) if this.calls(c).contains(s) =>  throw new IllegalArgumentException
        case (c, s) => this.calls += c -> (this.calls(c) + (s -> result))
      }


      override def getEvaluationsMapFromCall(call: String): Map[String, Int] = this.calls(call).filter(_._2.evaluation.isDefined).map(e=>(e._1,e._2.evaluation.get))

      //override def getResultsMapFromStudent(student: String): Map[String, String] = this.calls.flatMap(e => e._2.filter(_._1 == student).map(c => (e._1, c._2.toString)))
      override def getResultsMapFromStudent(student: String): Map[String, String] = this.calls.filter(_._2.contains(student)).map(e=>(e._1,e._2(student).toString))

      override def getBestResultFromStudent(student: String): Option[Int] = this.calls.values.flatten.filter(_._1 == student).map(_._2).map(_.evaluation).max

      override def getAllStudentsFromCall(call: String): Set[String] = this.calls(call).keySet
    }
  }
}
