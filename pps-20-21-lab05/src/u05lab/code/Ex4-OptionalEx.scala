package u05lab.code

import scala.:+

object ListUtility{
  def sequence[A](e: List[Option[A]]): Option[List[A]] = e match {
    case Some(h) :: t => t.foldLeft[Option[List[A]]](Some(List(h)))({
      case (Some(a), Some(b)) => Some(a.append(List(b)))
      case _ => None
    })
    case _ => None
  }
}