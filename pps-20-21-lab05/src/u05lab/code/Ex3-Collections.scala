package u05lab.code

import java.util.concurrent.TimeUnit
import scala.+:
import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {

  /* Linear sequences: List, ListBuffer */

  import scala.collection.mutable.ListBuffer

  //create
  val listBuffer: ListBuffer[Int] = ListBuffer(1, 2, 3)
  println(listBuffer.getClass)
  //read
  println(listBuffer.head)
  println(listBuffer(1))
  println(listBuffer.tail) //rest of collection without head
  //Update
  println(listBuffer(0) = 5)
  println(listBuffer :+ 4) //concatenate
  //listBuffer += 4 //append
  println(listBuffer.append(4)) //append
  listBuffer.update(0, 10)
  println(listBuffer) // insert
  //Delete
  println(listBuffer -= 10) //remove (subtract)
  println(listBuffer.subtractOne(10)) //remove
  print(listBuffer.toList) // to immutable list

  import scala.collection.immutable.List

  var list: List[Int] = List(1, 2, 3)
  println()
  println(list.getClass)
  //read
  println(list.head)
  println(list(1))
  println(list.tail)
  //Update
  println(list :+ 4) //concatenate
  println(list.appended(4)) //concat
  println(list.updated(0, 10)) // insert
  //Delete
  println(list.diff(List(10))) //difference


  /* Indexed sequences: Vector, Array, ArrayBuffer */

  import scala.collection.immutable.Vector

  val vector: Vector[Int] = Vector(1, 2, 3)
  println()
  println(vector.getClass)
  //read
  println(vector.head)
  println(vector(1))
  println(vector.tail)
  //Update
  println(vector :+ 4) //concatenate
  println(vector.appended(4)) //concat
  println(vector.updated(0, 10)) // insert
  //Delete
  println(vector.diff(List(10))) //difference

  import scala.collection.mutable.ArrayBuffer

  val arrayBuffer: ArrayBuffer[Int] = ArrayBuffer(1, 2, 3)
  println()
  println(arrayBuffer.getClass)
  //read
  println(arrayBuffer.head)
  println(arrayBuffer(1))
  println(arrayBuffer.tail)
  //Update
  println(arrayBuffer(0) = 5)
  println(arrayBuffer :+ 4) //append
  println(arrayBuffer.appended(4)) //append
  println(arrayBuffer.updated(0, 10)) // insert
  //Delete
  println(arrayBuffer.diff(List(10))) //difference

  val array: Array[Int] = Array(1, 2, 3)
  println()
  println(array.getClass)
  //read
  println(array.head)
  println(array(1))
  //Update
  array(0) = 5
  println(array) // strange behaviour

  /* Sets */
  var set: Set[Int] = Set(1, 2, 3)
  println()
  println(set.getClass)
  //read
  println(set.head)
  println(set(1))
  println(set.tail)
  //Update
  println(set + 5) //concat
  set += 4 //append side effect
  println(set)
  //Delete
  println(set - 1) // remove
  println(set &~ Set(1, 2)) //difference

  import scala.collection._

  var mutableSet: mutable.Set[Int] = mutable.Set(1, 2, 3)
  println()
  println(mutableSet.getClass)
  //read
  println(mutableSet.head)
  println(mutableSet(1)) //contains
  println(mutableSet.tail)
  //Update
  println(mutableSet += 4) //append
  //Delete
  println(mutableSet -= 1) // remove
  println(mutableSet &~ Set(2, 3)) //difference

  /* Maps */
  var map: Map[Int, String] = Map(1 -> "a", 2 -> "b", 3 -> "c")
  println()
  println(map.getClass)
  //read
  println(map(1))
  println(map contains 3) //contains
  //Update
  println(map map { case (k, v) => (v, k) }) //map function

  import collection._

  val mutableMap: mutable.Map[Int, String] = mutable.Map[Int, String](1 -> "a", 2 -> "b", 3 -> "c")
  println()
  println(map.getClass)
  //read
  println(map(1))
  println(map contains 3) //contains
  //Update
  println(mutableMap += (4 -> "d")) //concat
  mutableMap(4) = "e"
  println(mutableMap) //update
  //Delete
  println(mutableMap -= 4) //remove item

}
  /* Comparison */
  //Seq
object CollectionsComparison extends App{

    import PerformanceUtils._
    val maxValue:Int = 1_000_000
    val list = (1 to maxValue).toList
    val vector = (1 to maxValue).toVector
    val array = (1 to maxValue).toArray
    println()
    assert( measure("list last"){ list.last } > measure("vector last"){ vector.last } )
    assert( measure("list append"){ list :+ 0 } > measure("vector append"){ vector :+ 0 } )
    assert( measure("list prepend"){ list.prepended(0) } < measure("vector prepend"){ vector.prepended(0) } )
    assert( measure("list apply"){ list(list.size/2) } > measure("vector apply"){ vector(vector.size/2) } )
    println()
    assert( measure("array last"){ array.last } > measure("vector last"){ vector.last } )
    assert( measure("array append"){ array :+ 0 } > measure("vector append"){ vector :+ 0 } )
    assert( measure("list prepend"){ array.prepended(0) } > measure("vector prepend"){ vector.prepended(0) } )
    assert( measure("array apply"){ array(array.length/2) } < measure("vector apply"){ vector(vector.size/2) } )
    println()
    assert( measure("array last"){ array.last } < measure("list last"){ list.last } )
    assert( measure("array append"){ array :+ 0 } < measure("list append"){ list :+ 0 } )
    assert( measure("array prepend"){ array.prepended(0) } > measure("list prepend"){ list.prepended(0) } )
    assert( measure("array apply"){ array(array.length/2) } < measure("list apply"){ list(list.size/2) })

    import scala.collection.immutable.{HashSet,TreeSet}
    val immutableHashSet:Set[Int] = HashSet(1 to maxValue: _*)
    val immutableTreeSet:Set[Int] = TreeSet(1 to maxValue: _*)
    val mutableSet:mutable.Set[Int] = mutable.Set(1 to maxValue: _*)
    println()
    assert( measure("immutableHashSet add"){ immutableHashSet + 0 } < measure("immutableTreeSet add"){ immutableTreeSet + 0 } )
    assert( measure("immutableHashSet max"){ immutableHashSet.max } > measure("immutableTreeSet max"){ immutableTreeSet.max } )
    assert( measure("immutableHashSet contains"){ immutableHashSet(maxValue/2)} > measure("immutableTreeSet contains"){ immutableTreeSet(maxValue/2) } )
    println()
    assert( measure("mutableSet add"){ mutableSet += 0 } < measure("immutableTreeSet add"){ immutableTreeSet + 0 } )
    assert( measure("mutableSet max"){ mutableSet.max } > measure("immutableTreeSet max"){ immutableTreeSet.max } )
    assert( measure("mutableSet contains"){ mutableSet(maxValue/2)} > measure("immutableTreeSet contains"){ immutableTreeSet(maxValue/2) } )
    println()
    assert( measure("mutableSet add"){ mutableSet += 0 } > measure("immutableHashSet add"){ immutableTreeSet + 0 } )
    assert( measure("mutableSet max"){ mutableSet.max } > measure("immutableHashSet max"){ immutableTreeSet.max } )
    assert( measure("mutableSet contains"){ mutableSet(maxValue/2)} > measure("immutableHashSet contains"){ immutableTreeSet(maxValue/2) } )

    import scala.collection.immutable.{HashMap, TreeMap}
    val immutableHashMap:Map[Int,Int] = HashMap((1 to maxValue).zipWithIndex: _*)
    val immutableTreeMap:Map[Int,Int] = TreeMap((1 to maxValue).zipWithIndex: _*)
    val mutableMap:mutable.Map[Int,Int] = mutable.Map((1 to maxValue).zipWithIndex: _*)
    println()
    assert( measure("immutableHashMap add"){ immutableHashMap + (0->0) } < measure("immutableTreeMap add"){ immutableTreeMap + (0->0) } )
    assert( measure("immutableHashMap max"){ immutableHashMap.max } > measure("immutableTreeMap max"){ immutableTreeMap.max } )
    assert( measure("immutableHashMap contains"){ immutableHashMap(maxValue/2)} > measure("immutableTreeMap contains"){ immutableTreeMap(maxValue/2) } )
    println()
    assert( measure("mutableMap add"){ mutableMap += (0->0) } < measure("immutableTreeMap add"){ immutableTreeMap + (0->0) } )
    assert( measure("mutableMap max"){ mutableMap.max } > measure("immutableTreeMap max"){ immutableTreeMap.max } )
    assert( measure("mutableMap contains"){ mutableMap(maxValue/2)} > measure("immutableTreeMap contains"){ immutableTreeMap(maxValue/2) } )
    println()
    assert( measure("mutableMap add"){ mutableMap += (0->0)} < measure("immutableHashMap add"){ immutableHashMap + (0->0) } )
    assert( measure("mutableMap max"){ mutableMap.max } > measure("immutableHashMap max"){ immutableHashMap.max } )
    assert( measure("mutableMap contains"){ mutableMap(maxValue/2)} > measure("immutableHashMap contains"){ immutableHashMap(maxValue/2) } )
  }