package u05lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u05lab.code.List._

class Ex1Tests {

  val list: List[String] = List("a","b","c")
  val numberList: List[Int] = 1 :: 2 :: 3 :: nil
  val condition: Int => Boolean = i => i > 0
  val function: PartialFunction[Int,Int] = {case x if x<15 || x>35 => x-1}

  @Test
  def testZipRightOnEmptyList() {
    assertEquals(List.nil, List.nil.zipRight)
  }

  @Test
  def testZipRightListWithValues() {
    assertEquals(List(("a", 0), ("b", 1), ("c", 2)), list.zipRight)
  }

  @Test
  def testPartitionOnEmptyList(): Unit ={
    assertEquals((nil,nil),List.nil[Int].partition(condition))
  }

  @Test
  def testPartitionListWithValues(): Unit ={
    assertEquals((1::2::3::nil,(-3)::(-2)::(-1)::nil),((-3)::(-2)::(-1)::1::2::3::nil) .partition(condition))
  }

  @Test
  def testSpanOnEmptyList(): Unit ={
    assertEquals((nil,nil),nil[Int].span(_ == 3))
  }

  @Test
  def testSpanListWithValues(): Unit ={
    assertEquals((nil,numberList),numberList.span(_ == 3))
  }

  @Test
  def testReduceSumOnList(): Unit ={
    assertEquals(6, numberList.reduce(_+_))
  }

  @Test
  def testReduceOnHead(): Unit ={
    val head = 1
    assertEquals(head, (head :: nil).reduce(_+_))
  }

  @Test
  def testReduceEmptyList(): Unit ={
    assertThrows(classOf[UnsupportedOperationException],() => List.nil[Int].reduce(_+_))
  }

  @Test
  def testTakeRightEmptyList(): Unit ={
    assertEquals(nil,nil.takeRight(5))
  }

  @Test
  def testTakeWithValues() {
    assertEquals(2::3::nil, numberList.takeRight(2))
  }

  @Test
  def testTakeWithValuesExceedingSize() {
    assertEquals(numberList, numberList.takeRight(5))
  }

  @Test
  def testCollectEmptyList(): Unit ={
    assertEquals(nil,nil.collect(function))
  }

  @Test
  def testCollectWithValues(): Unit ={
    assertEquals(9::39::nil,(10::20::30::40::nil).collect(function))
  }

  @Test
  def testCollectWithIntegerValues(): Unit ={
    assertEquals(9::39::nil,(10::20::30::40::nil).collect(function))
  }

  @Test
  def testCollectNoMatch(): Unit ={
    assertEquals(nil,(1::1::1::nil).collect({case x if x > 10 => x.toString}))
  }

  @Test
  def testCollectIntToString(): Unit ={
    assertEquals("1"::"2"::"3"::nil,numberList.collect({case x if x < 10 => x.toString}))
  }
}