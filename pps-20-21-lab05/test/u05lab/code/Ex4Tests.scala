package u05lab.code

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u05lab.code.ListUtility.sequence


class Ex4Tests {

  @Test
  def testSequenceEmptyList(): Unit ={
    assertEquals(None,sequence(List.nil[Option[Int]]))
  }

  @Test
  def testSequenceNoneInList(): Unit ={
    assertEquals(None,sequence(List[Option[Int]](Some(1),None,Some(3))))
  }

  @Test
  def testSequenceNoNoneInList(): Unit ={
    assertEquals(Some(List(1, 2, 3)),sequence(List[Option[Int]](Some(1),Some(2),Some(3))) )
  }

}
