package lab01.example.model;

import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;

public class SimpleBankAccountWithATM extends SimpleBankAccount {

    private static final int FEE = 1;

    public SimpleBankAccountWithATM(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    @Override
    public void deposit(int usrID, double amount) {
        super.deposit(usrID,amount-FEE);
    }

    @Override
    public void withdraw(int usrID, double amount) {
        super.withdraw(usrID,amount+FEE);
    }
}
