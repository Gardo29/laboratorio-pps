import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
public abstract class AbstractBankAccountTest {
    protected AccountHolder accountHolder;
    protected BankAccount bankAccount;

    @BeforeEach
    void beforeEach(){
        accountHolder = new AccountHolder("Mario", "Rossi", 1);
        bankAccount = getBankAccount();
    }

    @Test
    void testInitialBalance() {
        assertEquals(0, bankAccount.getBalance());
    }


    @Test
    void testDeposit(){
        bankAccount.deposit(accountHolder.getId(), 100);
        assertEquals(this.expectedDeposit(),bankAccount.getBalance());
    }

    @Test
    void testWrongDeposit(){
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.deposit(2, 50);
        assertEquals(this.expectedWrongDeposit(), bankAccount.getBalance());
    }

    @Test
    void testWithdraw(){
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdraw(accountHolder.getId(), 70);
        assertEquals(this.expectedWithdraw(), bankAccount.getBalance());
    }

    @Test
    void testWrongWithdraw(){
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdraw(2, 70);
        assertEquals(this.expectedWrongWithdraw(), bankAccount.getBalance());
    }

    abstract public BankAccount getBankAccount ();
    abstract int expectedDeposit();
    abstract int expectedWrongDeposit();
    abstract int expectedWithdraw();
    abstract int expectedWrongWithdraw();
}
