import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import lab01.example.model.SimpleBankAccount;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the SimpleBankAccount implementation
 */
class SimpleBankAccountTest extends AbstractBankAccountTest{


    @Override
    public BankAccount getBankAccount() {
        return new SimpleBankAccount(this.accountHolder,0);
    }

    @Override
    int expectedDeposit() {
        return 100;
    }

    @Override
    int expectedWrongDeposit() {
        return 100;
    }

    @Override
    int expectedWithdraw() {
        return 30;
    }

    @Override
    int expectedWrongWithdraw() {
        return 100;
    }
}
