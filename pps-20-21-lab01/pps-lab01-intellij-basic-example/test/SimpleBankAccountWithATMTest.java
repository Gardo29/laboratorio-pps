import lab01.example.model.BankAccount;
import lab01.example.model.SimpleBankAccountWithATM;
import org.junit.jupiter.api.Test;

public class SimpleBankAccountWithATMTest extends AbstractBankAccountTest{
    @Override
    public BankAccount getBankAccount() {
        return new SimpleBankAccountWithATM(this.accountHolder,0);
    }

    @Override
    int expectedDeposit() {
        return 99;
    }

    @Override
    int expectedWrongDeposit() {
        return 99;
    }

    @Override
    int expectedWithdraw() {
        return 28;
    }

    @Override
    int expectedWrongWithdraw() {
        return 99;
    }
}
