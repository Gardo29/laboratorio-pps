import lab01.tdd.CircularList;
import lab01.tdd.CircularListImpl;
import lab01.tdd.SelectStrategy;
import lab01.tdd.select.strategy.SelectStrategyFactory;
import lab01.tdd.select.strategy.impl.SelectStrategyFactoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private static final int SUPPORT_VALUES_NUMBER = 10;
    private final List<Integer> supportValues = IntStream.range(0, SUPPORT_VALUES_NUMBER).boxed().collect(Collectors.toList());
    private CircularList circularList;
    private final SelectStrategyFactory factory = new SelectStrategyFactoryImpl();

    @BeforeEach
    public void beforeEach(){
        this.circularList = new CircularListImpl();
        Collections.shuffle(this.supportValues);
    }

    /*   emptiness tests   */
    @Test
    public void testInitialEmpty() { this.assertEmptyCircularList();}
    @Test
    public void testEmptyNextWithEmptyList() {
        this.assertEmptyCircularList();
        assertTrue(this.circularList.next().isEmpty());
    }
    @Test
    public void testEmptyNextWithMultipleOfStrategyAndEmptyList() {
        this.assertEmptyCircularList();
        assertTrue(this.circularList.next(this.factory.multipleOfStrategy(2)).isEmpty());
    }
    @Test
    public void testEmptyPrevWithEmptyList() {
        this.assertEmptyCircularList();
        assertTrue(this.circularList.previous().isEmpty());
    }
    @Test
    public void testEmptyOnResetWithEmptyList() {
        this.assertEmptyCircularList();
        this.circularList.reset();
        assertTrue(this.circularList.isEmpty());
    }
    @Test
    public void testNotEmptyWithOneAdd(){
        this.assertEmptyCircularList();
        this.circularList.add(this.getFirstSupportElement());
        assertFalse(this.circularList.isEmpty());
    }

    @Test
    public  void testNotEmptyWitMultipleAdds(){
        this.fill(this.supportValues);
        assertFalse(this.circularList.isEmpty());
    }

    /* add tests */
    @Test
    public void testSingleAddValue() {
        this.circularList.add(this.getFirstSupportElement());
    }

    @Test
    public void testMultipleAdds(){
        this.fill(this.supportValues);
    }

    /* next tests */
    @Test
    public void testNextSingleValue() {
        final int value = this.getFirstSupportElement();
        this.circularList.add(value);
        this.optionalEqualityCheck(value,this.circularList.next());
    }

    @Test
    public void testNextMultipleValues(){
        this.fill(this.supportValues);
        this.supportValues.forEach(v->this.optionalEqualityCheck(v,this.circularList.next()));
    }
    /* previous tests */
    @Test
    public void testPrevSingleValue(){
        this.fill(this.supportValues);
        this.optionalEqualityCheck(this.supportValues.get(this.supportValues.size()-1),this.circularList.previous());
    }
    @Test
    public void testPrevMultipleValue(){
        this.fill(this.supportValues);
        Collections.reverse(this.supportValues);
        this.supportValues.forEach(e->this.optionalEqualityCheck(e,this.circularList.previous()));
    }
    /*  size tests */
    @Test
    public void testInitialSize(){
        assertEquals(0,this.circularList.size());
    }
    @Test
    public void testSizeAfterMultipleAdds(){
        this.fill(this.supportValues);
        assertEquals(this.supportValues.size(),this.circularList.size());
    }
    @Test
    public void testSizeMultipleAdd(){
        supportValues.forEach(i->this.circularList.add(i));
        assertEquals(supportValues.size(),this.circularList.size());
    }
    /* pacman effect */
    @Test
    public void testCycleFromRightToLeft(){
        this.fill(this.supportValues);
        this.supportValues.forEach(e -> this.circularList.next());
        this.optionalEqualityCheck(this.getFirstSupportElement(),this.circularList.next());
    }
    @Test
    public void testCycleFromLeftToRight(){
        this.fill(this.supportValues);
        this.optionalEqualityCheck(this.supportValues.get(this.supportValues.size()-1),this.circularList.previous());
    }
    /* test selectStrategy */
    @Test
    public void testEvenStrategy(){
        List<Integer> oneEven = List.of(1,3,5,6);
        this.fill(oneEven);
        this.optionalEqualityCheck(6,this.circularList.next(this.factory.evenStrategy()));
    }
    @Test
    public void testMultipleStrategy(){
        List<Integer> oneMultiple = List.of(1,2,3,7,11,13,25);
        this.fill(oneMultiple);
        this.optionalEqualityCheck(25,this.circularList.next(this.factory.multipleOfStrategy(5)));
    }
    @Test
    public void testEqualStrategy(){
        List<Integer> oneEven = List.of(1,2,4,5,7,8,9);
        this.fill(oneEven);
        this.optionalEqualityCheck(9,this.circularList.next(this.factory.equalsStrategy(9)));
    }
    @Test
    public void testEvenPacmanEffectNextWithEvenStrategy(){
        List<Integer> firstEven = List.of(2,1,1,1);
        this.fill(firstEven);
        this.circularList.next();
        this.optionalEqualityCheck(2,this.circularList.next(this.factory.evenStrategy()));
    }

    /* test reset */
    @Test
    public void testCanDoResetEmptyList(){
        this.assertEmptyCircularList();
        this.circularList.reset();
    }
    @Test
    public void testResetAfterAdd(){
        this.fill(this.supportValues);
        this.circularList.reset();
        this.optionalEqualityCheck(this.getFirstSupportElement(),this.circularList.next());
    }
    @Test
    public void testResetAfterDoubleNext(){
        this.fill(this.supportValues);
        this.circularList.next();
        this.circularList.next();
        this.circularList.reset();
        this.optionalEqualityCheck(this.getFirstSupportElement(),this.circularList.next());
    }
    @Test
    public void testResetWithMultipleOfStrategy(){
        this.fill(this.supportValues);
        this.circularList.next(this.factory.multipleOfStrategy(3));
        this.circularList.reset();
        this.optionalEqualityCheck(this.getFirstSupportElement(),this.circularList.next());
    }


    /* utility functions */
    private Integer getFirstSupportElement(){
        return this.supportValues.get(0);
    }
    private void assertEmptyCircularList(){
        assertTrue(this.circularList.isEmpty());
    }

    private void optionalEqualityCheck(int expected, Optional<Integer> value){
        value.ifPresentOrElse(v->assertEquals(expected,v), Assertions::fail);
    }

    private void fill(Collection<Integer> collection){
        collection.forEach(i->this.circularList.add(i));
    }
}
