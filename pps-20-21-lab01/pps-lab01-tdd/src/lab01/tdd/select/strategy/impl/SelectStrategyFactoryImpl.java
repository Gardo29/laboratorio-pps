package lab01.tdd.select.strategy.impl;

import lab01.tdd.SelectStrategy;
import lab01.tdd.select.strategy.SelectStrategyFactory;

public class SelectStrategyFactoryImpl implements SelectStrategyFactory {

    @Override
    public SelectStrategy evenStrategy() {
        return i-> i%2 == 0;
    }

    @Override
    public SelectStrategy multipleOfStrategy(int value) {
        return i-> i%value == 0;
    }

    @Override
    public SelectStrategy equalsStrategy(int value) {
        return i->i==value;
    }
}
