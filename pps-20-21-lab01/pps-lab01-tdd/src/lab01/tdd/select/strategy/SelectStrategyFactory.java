package lab01.tdd.select.strategy;

import lab01.tdd.SelectStrategy;

public interface SelectStrategyFactory {
    public SelectStrategy evenStrategy();

    public SelectStrategy multipleOfStrategy(int value);

    public SelectStrategy equalsStrategy(int value);
}
