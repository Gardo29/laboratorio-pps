package lab01.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CircularListImpl implements CircularList {

    private static final int NEXT = +1;
    private static final int PREV = -1;
    private final List<Integer> list = new ArrayList<>();
    private Integer currentPosition = null;

    @Override
    public void add(int element) {
        this.list.add(element);
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public Optional<Integer> next() {
        return this.gelElement(NEXT);
    }

    @Override
    public Optional<Integer> previous() {
        return this.gelElement(PREV);
    }

    @Override
    public void reset() {
        this.currentPosition = null;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        if(this.currentPosition == null){
            this.currentPosition = 0;
        }
        return Stream.concat(IntStream.range(this.currentPosition,this.size()).boxed(), IntStream.range(0,this.currentPosition).boxed())
                .filter(i-> strategy.apply(this.list.get(i)))
                .map(this.list::get)
                .findFirst();
    }
    private int computeNextCurrentPosition(int shift){
        if(shift == PREV){
            this.currentPosition = this.currentPosition == null || this.currentPosition == 0 ? this.lastElement() : this.currentPosition-1;
        }
        if(shift == NEXT){
            this.currentPosition = this.currentPosition == null || this.currentPosition == this.lastElement() ? 0 : this.currentPosition+1;
        }
        return this.currentPosition;
    }

    private Optional<Integer> gelElement(int shift){
        return this.isEmpty() ? Optional.empty() : Optional.of(this.list.get(this.computeNextCurrentPosition(shift)));
    }

    private int lastElement() {
        return this.list.size()-1;
    }
}
