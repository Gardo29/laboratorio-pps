package u04lab.code

import u04lab.code.Lists.List
import u04lab.code.Lists.List.{Cons, foldLeft, map, nil}

object OptionalExercises {

  def List[A](elements:A*):List[A] = elements.foldRight(nil[A])(Cons[A])

  object sameTeacher{
    def unapply(courses: List[Course]): Option[String] = {
      map(courses)(_.teacher) match {
        case Cons(teacher,t) if foldLeft(t)(true)((acc, otherTeacher) =>  acc && otherTeacher == teacher) => Some(teacher)
        case _ => None
      }
    }
  }
}

object Bo {
  def unapply(arg1: String,arg2:String): Option[Int] = arg1 match {
    case arg if arg.length > 5 => Some(arg.length)
    case _ => None
  }
}

object ma extends App{
  "giovannino" match {
    case Bo(len) => print(len)
  }
}
