package u04lab.code

import Optionals.Option
import Option.{None, Some}
import Lists.List.{Nil, append, nil, reverse}
import Lists.List
import Streams.Stream
import u04lab.code.Streams.Stream.{Cons, take}

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  private case class PowerIteratorImpl[A](private var stream: Stream[A]) extends PowerIterator[A]{
    private var _parList: List[A] = nil

    override def next(): Option[A] = stream match {
      case Cons(head, tail) =>
        val headValue = head()
        _parList = append(_parList,List.Cons(headValue,nil))
        stream = tail()
        Option.of(headValue)
      case _ => Option.empty
    }
    override def allSoFar(): List[A] = _parList

    override def reversed(): PowerIterator[A] = new PowerIteratorsFactoryImpl().fromList(reverse(_parList))
  }

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIteratorImpl(Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = PowerIteratorImpl(Stream.fromList(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIteratorImpl(take(Stream.generate(Random.nextBoolean()))(size))
}
