package u04lab.code

import org.junit.jupiter.api.Assertions.{assertEquals, assertTrue, fail}
import org.junit.jupiter.api.Test
import u04lab.code.OptionalExercises.{List, sameTeacher}
import u04lab.code.Lists.List._

class OptionalExercisesTest {

  @Test
  def testFactoryList(): Unit ={
    assertEquals(Cons(1,Cons(2,Cons(3,nil))),List(1,2,3))
  }

  @Test
  def testFactoryEmptyList(): Unit ={
    assertEquals(nil,List())
  }

  @Test
  def testSameTeacherNoValue(): Unit ={
    val differentTeachers = List(Course("PCD","Ricci"),Course("Reti","Pau"))

    differentTeachers match {
      case sameTeacher("Ricci") => fail()
      case _ => assertTrue(true)
    }
  }

  @Test
  def testSameTeacherWithValue(): Unit ={
    val equalsTeacher = List(Course("IOT","Ricci"),Course("PCD","Ricci"))

    equalsTeacher match {
      case sameTeacher("Ricci") => assertTrue(true)
      case _ => fail()
    }
  }

}
