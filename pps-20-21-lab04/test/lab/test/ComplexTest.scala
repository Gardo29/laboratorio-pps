package lab.test


import org.junit.jupiter.api.Assertions.{assertAll, assertEquals, assertNotEquals}
import org.junit.jupiter.api.{Assertions, Test}
import u04lab.code.Complex

class ComplexTest {
  val complexNumber: Complex = Complex(5,4)

  /*@Test
  def testDefaultEqualsNotWorkingClass(): Unit ={
    assertNotEquals(complexNumber,Complex(5,4))
  }

  @Test
  def testDefaultToStringNotWorkingClass(): Unit ={
    assertNotEquals("Complex(5,4)",complexNumber.toString)
  }*/
  @Test
  def testSumOfComplexNumbers(): Unit ={
    assertEquals(Complex(12,-4),complexNumber + Complex(7,-8))
  }

  @Test
  def testProductOfComplexNumbers(): Unit ={
    assertEquals(Complex(67,-12),complexNumber * Complex(7,-8))
  }

  @Test
  def testDefaultEqualsWorkingCaseClass(): Unit ={
    assertEquals(complexNumber,Complex(5,4))
  }

  @Test
  def testDefaultToStringWorkingCaseClass(): Unit ={
    assertEquals("ComplexImpl(5,4)",complexNumber.toString)
  }

}
