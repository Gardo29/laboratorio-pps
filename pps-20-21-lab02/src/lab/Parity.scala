package lab

object Parity {
  def parity: Int => String = x => x % 2 match {
    case 1 => "odd"
    case _ => "even"
  }
}
