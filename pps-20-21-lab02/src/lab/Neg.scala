package lab

object Neg {
  def neg[A]: (A => Boolean) => (A => Boolean) = f => (s => !f(s))
}
