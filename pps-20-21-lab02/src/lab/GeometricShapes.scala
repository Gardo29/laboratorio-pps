package lab

object GeometricShapes {

  sealed trait Shape

  object Shape {

    case class Rectangle(larger_side: Double, smaller_side: Double) extends Shape

    case class Circle(radius: Double) extends Shape

    case class Square(side: Double) extends Shape

    def area(shape: Shape): Double = shape match {
      case Rectangle(larger, smaller) => larger * smaller
      case Circle(radius) => radius * radius * Math.PI
      case Square(side) => side * side
    }

    def perimeter(shape: Shape): Double = shape match {
      case Rectangle(larger, smaller) => larger * 2 + smaller * 2
      case Circle(radius) => 2 * radius * Math.PI
      case Square(side) => side * 4
    }
  }

}
