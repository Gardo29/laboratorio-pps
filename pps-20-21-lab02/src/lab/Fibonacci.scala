package lab

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

object Fibonacci {

  def fib(n: Int): Int = {
    @annotation.tailrec
    def _step(n: Int, prev: Int, current: Int): Int = n match {
      case n if (n <= 0) => current
      case _ => _step(n - 1, prev + current, prev)
    }

    _step(n, 1, 0)
  }

  @Test def testFibonacci() {
    assertEquals((0, 1, 1, 2, 3), (fib(0), fib(1), fib(2), fib(3), fib(4)))
  }

}
