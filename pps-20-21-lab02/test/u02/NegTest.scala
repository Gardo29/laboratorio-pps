package u02

import org.junit.jupiter.api.Assertions.{assertEquals, assertFalse, assertTrue}
import org.junit.jupiter.api.Test
import lab.Neg.neg

class NegTest {
  val empty: String => Boolean = _==""
  val notEmpty = neg(empty)
  
  @Test def testNeg(){

    assertTrue(notEmpty("non empty string"))
    assertFalse(notEmpty(""))
    assertTrue(notEmpty("foo") && !notEmpty(""))
  }


}
