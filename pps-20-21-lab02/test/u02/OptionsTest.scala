package u02

import lab.Optionals._
import lab.Optionals.Option._
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.{BeforeEach, Test}

class OptionsTest {
  val s1: Option[Int] = Some(1)
  val s2: Option[Int] = Some(2)
  val s3: Option[Int] = None()

  @Test
  def TestSome(){
    assertEquals(Some(1),s1)
  }
  @Test
  def testGetOrElse(){
    assertEquals(1, getOrElse(s1, 0))
    assertEquals(0, getOrElse(s3, 0))
  }
  @Test
  def testFlatMap(){
    assertEquals(Some(2),flatMap(s1)(i => Some(i + 1)))
  }
  @Test
  def testCombineSome(){
    assertEquals(Some(3),flatMap(s1)(i => flatMap(s2)(j => Some(i + j))))
  }
  @Test
  def testFlatMapNone(){
    assertNone(flatMap(s1)(i => flatMap(s3)(j => Some(i + j))))
  }

  @Test
  def testFilterSomeInt(){
    assertEquals(Some(5),filter(Some(5))(_ > 2))
  }

  @Test
  def testFilterSomeIntWithNoneResult(){
    assertNone(filter(Some(5))(_ > 8))
  }

  @Test
  def testMapOnNone(){
    assertNone(map(None())(_ == 8))
  }

  @Test
  def testMapFromIntToString(){
    assertEquals(Some("2"),map(s2)(_.toString()))
  }

  @Test
  def testMap2FromIntAndStringToBoolean(){
    assertEquals(Some(true),map2(s2)(Some("2"))((a,b)=> a.toString == b))
  }

  @Test
  def testMap2FromIntAndNoneToNone(){
    assertNone(map2(s2)(s3)(_+_))
  }

  @Test
  def testMap2FromNoneAndNoneToNone(){
    assertNone(map2(s3)(s3)(_ == _))
  }

  def assertNone[A](v:Option[A]){
    assertEquals(s3,v)
  }

}
