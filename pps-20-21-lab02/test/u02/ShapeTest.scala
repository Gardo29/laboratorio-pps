package u02

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import lab.GeometricShapes.Shape
import lab.GeometricShapes.Shape.{Circle, Rectangle, Square, area, perimeter}


class ShapeTest {

  @Test def testAreaRectangle(){
    assertEquals(35,area(Rectangle(7,5)))
  }
  @Test def testAreaCircle(){
    assertEquals(Math.PI,area(Circle(1)))
  }
  @Test def testAreaSquare(){
    assertEquals(25,area(Square(5)))
  }
  @Test def testPerimeterRectangle(){
    assertEquals(30,perimeter(Rectangle(10,5)))
  }
  @Test def testPerimeterCircle(){
    assertEquals(2*Math.PI,perimeter(Circle(1)))
  }
  @Test def testPerimeterSquare(){
    assertEquals(20,perimeter(Square(5)))
  }
}
