package u02

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import lab.Parity.parity

class ParityTest {
  @Test def testParity(){
    assertEquals("odd",parity(55))
    assertEquals("even",parity(56))
  }
}
