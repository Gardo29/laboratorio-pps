package u02

import lab.VarietyFunctions.{p1, p2, p3, p4}
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class VarietyFunctionsTest {
  @Test def testTrueVarNoCurrying() {
    assertTrue(p1(1)(2)(3))
  }

  @Test def testTrueVarCurrying() {
    assertTrue(p2(1, 2, 3))
  }

  @Test def testTrueDefNoCurrying() {
    assertTrue(p3(1)(2)(3))
  }

  @Test def testTrueDefCurrying() {
    assertTrue(p4(1, 2, 3))
  }
}
