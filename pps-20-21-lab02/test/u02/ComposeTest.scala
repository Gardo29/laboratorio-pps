package u02

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import lab.FunctionalComposition.compose


class ComposeTest {

  @Test
  def composeTest(){
    assertEquals(9, compose((i:Int) => i -1,(i:Int) => i * 2)(5))
  }

}
